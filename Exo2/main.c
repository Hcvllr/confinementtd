
#include <stdio.h>
#include <stdlib.h>

#include "functions.h"

int main() {
    list liste_1 = createList(-5);
    listAppend(liste_1, -11);
    listAppend(liste_1, 3);
    listAppend(liste_1, 26);
    listAppend(liste_1, 4);
    listAppend(liste_1, -2);
    listAppend(liste_1, 4);
    listAppend(liste_1, -8);
    listAppend(liste_1, 9);
    listAppend(liste_1, -4);

    
    printList(liste_1);
    

    printf("\n\nRemoval of prime numbers\n\n");
    removeWhere(liste_1, isPrime);
    printList(liste_1);
    
    printf("\n\nRemoval of odd numbers\n\n");
    removeWhere(liste_1, isOdd);
    printList(liste_1);

    printf("\n\nRemoval of negative numbers\n\n");
    removeWhere(liste_1, isNegative);
    printList(liste_1);
    
    freeList(liste_1);
    return 0;
}
