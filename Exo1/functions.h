//Function replacing value of an int by double
void fois_deux(int* n);

//Function replacing value of an int by half
void divise_deux(int* n);

//Print int to console giving its pointer
void printINT(int* var);

//Apply function to tab 
void apply(int* tab, int size, void (*func)(int*));


