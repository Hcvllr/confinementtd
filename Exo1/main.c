#include <stdio.h>
#include "functions.h"

int main() {
    
    int myArray[] = {0, 14, 5, 36};
    
    printf("Tableau initial : \n");
    apply(myArray, 4, printINT);
    
    apply(myArray, 4, fois_deux);
    
    printf("Tableau après la multiplication : \n");
    apply(myArray, 4, printINT);
    
    apply(myArray, 4, divise_deux);
    
    printf("Tableau après la division : \n");
    apply(myArray, 4, printINT);
    
    
    return 0;
}
